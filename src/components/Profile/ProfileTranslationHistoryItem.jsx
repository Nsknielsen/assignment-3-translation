//single translation item displaying the translation it represents
const ProfiletranslationHistoryItem = ({translation}) => {
    if (!translation.isDeleted){
        return (<li>{translation.text}</li>)
    } 
    return null
}

export default ProfiletranslationHistoryItem