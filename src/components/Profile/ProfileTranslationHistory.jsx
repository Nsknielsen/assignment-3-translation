import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ( {translations}) => {
//translations is a list of strings that we destructure from the props object above

//makes an array of jsx elements of the type ProfileTranslationHistoryItem for each of the translation strings.
const translationList = translations.map((translation, index) => <ProfileTranslationHistoryItem key={index + '-' + translation} translation={translation}/>)

    return (
        <section>
            <h4>Your translation history</h4>

            {translationList.length === 0 && <p>You have no translations yet</p>}
                <ul>
                    {translationList }
                </ul>
        </section>
    )
}

export default ProfileTranslationHistory