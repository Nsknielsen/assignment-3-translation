//Link component provided by react router to allow routing

import { useState } from "react"
import { Link } from "react-router-dom"

import { useUser } from "../../context/UserContext"

import { storageDelete, storageSave } from "../../utils/storage"

import { STORAGE_KEY_USER } from "../../const/storageKeys"

import { translationClearHistory } from "../../api/translation"

const ProfileActions = () => {

    //getting curr user from context using my useUser hook - to pass as prop to ProfileHeader below, and use in event handlers like for clearing history

    const { user, setUser } = useUser()

    const [apiError, setApiError] = useState(null)

    //handler for logging out

    const handleLogoutClick = () => {

        if (window.confirm('Are you sure?')) {

            //first delete the user in the local storage, then...

            storageDelete(STORAGE_KEY_USER)

            //... set user to null in the state too - userdata is persistent in the api, so we can safely remove it in these places

            setUser(null)

        }

    }

    //handler for clearing translation history

    const handleClearHistory = async () => {

        const deletedTranslations = []

        for (let translation of user.translations) {

            const deletedTranslation = {

                text: translation.text,

                isDeleted: true

            }

            deletedTranslations.push(deletedTranslation)

        }

        const [error, updatedUser] = await translationClearHistory(user, deletedTranslations)

        //note the below error handling needs local API error state like in TranslationView

        if (error !== null) {

            setApiError(error)

            return

        }

        setUser(updatedUser)
        storageSave(STORAGE_KEY_USER, updatedUser)
    }

    return (
        <ul>
            <li><Link to='/translations'>TRANSLATIONS</Link></li>
            <li><button onClick={handleClearHistory}>Clear history</button></li>
            <li><button onClick={handleLogoutClick}>Logout</button></li>
            {apiError && <p>{apiError}</p>}
        </ul>
    )
}

export default ProfileActions