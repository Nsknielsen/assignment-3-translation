import React from 'react';

//container component for the images used in a translation
const TranslationOutput = ({images}) => {

    return <div>
      <h2>Your text in American sign language:</h2>
      {images}
  </div>;
};

export default TranslationOutput;
