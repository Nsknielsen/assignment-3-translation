import withAuth from "../hoc/withAuth"
import TranslationInputForm from "../components/Translation/TranslationInputForm"
import { useState } from "react"
import { useUser } from "../context/UserContext"
import { translationAdd } from "../api/translation"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { LETTERS_REGEX } from "../const/regex"
import TranslationOutput from "../components/Translation/TranslationOutput"

const TranslationView = () => {

    //state for showing sing language images
    const [images, setImages] = useState([null])
    
    //list of images to display as a translation
    const imageObjects = []

    //local state for errors in api call
    const [apiError, setApiError] = useState(null)

    //getting current user from the custom hook made in usercontext
    const {user, setUser} = useUser()
    
    //event handler for the translate button in TranslationInput child. patches the latest translation to api, 
    //then calls generateImages to update DOM and updates the current user data
    const handleTranslateClicked = async translationInput => {
        const [error, updatedUser] = await translationAdd(user, translationInput)
        if (error !== null){
            setApiError(error)
             return
        }
        generateImages(translationInput)
        setUser(updatedUser)
        storageSave(STORAGE_KEY_USER, updatedUser)
    }

    //helper function to generate a list of image elements that fit the user input
    const generateImages = (inputText) => {
        for (let index = 0; index < inputText.length; index++){
            let letter = inputText[index]
            if (letter.match(LETTERS_REGEX)){
                imageObjects.push({image: `img/${letter}.png`,
                id: index})
            } else if (letter === ' '){
                imageObjects.push({image:'img/space.png', 
                id: index})
            }
        }
        setImages(imageObjects.map(imageObject => {
            return <img
            src={imageObject.image}
            width='50'
            key={imageObject.id}
            />
        }))
    }

    return (
        <>
            <h1>Translation</h1>
            <section id='translate-input'>
                <TranslationInputForm translateHandler={handleTranslateClicked} />
                {apiError && <p>{apiError}</p>}
            </section>

            <section id="translate-output">
            <TranslationOutput images={images}/>
            </section>
        </>
    )
}

//wraps the Translation view in the withAuth higher-order-component to check if user is logged in before showing this view
export default withAuth(TranslationView)