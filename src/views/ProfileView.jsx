import { useEffect } from "react"
import { userById } from "../api/user"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"

const ProfileView = () => {

    //getting curr user from context using my useUser hook - to pass as prop to ProfileHeader and translationHistory below, and use in useEffect to set user
    const { user, setUser } = useUser()

    //useEffect hook runs on mount and when user obj is updated. Used to update the local state and local storage versions of "translations" by asking the api if there has been an update - otherwise won't show new translations until after logout and login again
    //checks if current user with x id can be found in api, then updates user in state and local storage if appropriate
    useEffect(() => {
        const findUser = async () => {
            const [error, latestUser] = await userById(user.id)
            if (error === null){
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }

    }, [setUser, user.id])

    return (
        <>
            <h1>Profile</h1>
            <ProfileHeader username={user.username}></ProfileHeader>
            <ProfileActions></ProfileActions>
            <ProfileTranslationHistory translations={user.translations}></ProfileTranslationHistory>
        </>
        
    )
}

//wraps the Profile view in the withAuth higher-translation-component to check if user is logged in before showing this view
export default withAuth(ProfileView)