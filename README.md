# Description
This is a sign language translation website where users can log in and translate English text to American sign language. 
You can also see a history of your latest 10 translations.

# Developers
Anders Madsen, Liv Reinhold, Nikolaj Nielsen

# Acknowledgements
This project, especially the login and authentication functionality, is inspired by the Coffee Orders demo by Dewald Els.

# Netlify link
https://practical-goldwasser-9d826e.netlify.app/

# Dependencies
react
react-router-dom
react-hook-form
